# skyline-qs-extension


## Development

`npm install`

Edit `qext.config.json --> desktopConfig --> destination` to reflect the QS Desktop path

`npm run watch-qext-scripts`


## Install

* extract the zip file from the `dist` folder into the QS extensions folder `C:\Users\USERNAME\Documents\Qlik\Sense\Extensions`
* Copy the qvf file from `QVF` folder into the apps folder `C:\Users\USERNAME\Documents\Qlik\Sense\Apps`
* Open QS Desktop