export default function ($element, layout) {
	clearContainers()
	let qMatrix = layout.qHyperCube.qDataPages[0].qMatrix

	let store = {
		data: {
			arrivals: [],
			departures: []
		}
	}

	store.data.arrivals = qMatrix.filter(function (x) {
		return x[1].qText == 'ARRIVAL'
	})

	store.data.departures = qMatrix.filter(function (x) {
		return x[1].qText == 'DEPARTURE'
	})


	for (let dep of store.data.departures) {
		$(`.dh${dep[2].qText}`).append(`<div>${dep[0].qText}</div>`);
	}

	for (let arr of store.data.arrivals) {
		$(`.ah${arr[2].qText}`).append(`<div>${arr[0].qText}</div>`);
	}	
}

function clearContainers() {
	$('.departures').html(`        
	<div class="departures-hour identification-departure">DEPARTURES</div>
	<div class="departures-hour dh0500"></div>
	<div class="departures-hour dh0600"></div>
	<div class="departures-hour dh0700"></div>
	<div class="departures-hour dh0800"></div>
	<div class="departures-hour dh0800"></div>
	<div class="departures-hour dh1000"></div>
	<div class="departures-hour dh1100"></div>
	<div class="departures-hour dh1200"></div>
	<div class="departures-hour dh1300"></div>
	<div class="departures-hour dh1400"></div>
	<div class="departures-hour dh1500"></div>
	<div class="departures-hour dh1600"></div>
	<div class="departures-hour dh1700"></div>
	<div class="departures-hour dh1800"></div>
	<div class="departures-hour dh1900"></div>
	<div class="departures-hour dh2000"></div>
	<div class="departures-hour dh2100"></div>
	<div class="departures-hour dh2200"></div>
	<div class="departures-hour dh2300"></div>`)


	$('.arrivals').html(`        
	<div class="departures-hour identification-departure">ARRIVALS</div>
	<div class="arrivals-hour ah0500"></div>
	<div class="arrivals-hour ah0600"></div>
	<div class="arrivals-hour ah0700"></div>
	<div class="arrivals-hour ah0800"></div>
	<div class="arrivals-hour ah0800"></div>
	<div class="arrivals-hour ah1000"></div>
	<div class="arrivals-hour ah1100"></div>
	<div class="arrivals-hour ah1200"></div>
	<div class="arrivals-hour ah1300"></div>
	<div class="arrivals-hour ah1400"></div>
	<div class="arrivals-hour ah1500"></div>
	<div class="arrivals-hour ah1600"></div>
	<div class="arrivals-hour ah1700"></div>
	<div class="arrivals-hour ah1800"></div>
	<div class="arrivals-hour ah1900"></div>
	<div class="arrivals-hour ah2000"></div>
	<div class="arrivals-hour ah2100"></div>
	<div class="arrivals-hour ah2200"></div>
	<div class="arrivals-hour ah2300"></div>`)

}
