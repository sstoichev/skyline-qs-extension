export default {
	type: "items",
	component: "accordion",
    items: {
        dimensions: {
			uses: "dimensions",
			min: 3,
			max: 3
        },
        // measures: {
        //     uses: "measures"
        // },
        sorting: {
            uses: "sorting"
        },
        appearance: {
            uses: "settings"
        }
    }
}
